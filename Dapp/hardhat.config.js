require('@nomicfoundation/hardhat-toolbox');

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: '0.8.9',
  settings: {
    optimizer: {
      enabled: true,
      runs: 200,
    },
  },
  networks: {
    hardhat: {
      chainId: 1337,
      allowUnlimitedContractSize: true,
      accounts: [
        {
          privateKey: '40a20905cacfc7f174271928c79cee13ad836348c607a57f53165956399345e8',
          balance: '1000000000000000000000',
        },
        {
          privateKey: '0a9db9af4246054d1d5ba7d54c92fd279b188c6699748c81b9e25b1d17908198',
          balance: '2000000000000000000000',
        },
        {
          privateKey: '954e18470d7bfc53d965bccd652134c4e2a3125ebaf57d5f9e64a4c291611eb6',
          balance: '3000000000000000000000',
        },
      ],
    },
    ropsten: {
      url: 'https://ropsten.infura.io/v3/2cf3a3225bf14bd0a9a5342e75354536',
      accounts: ['0x40a20905cacfc7f174271928c79cee13ad836348c607a57f53165956399345e8'],
    },
    rinkeby: {
      url: 'https://rinkeby.infura.io/v3/2cf3a3225bf14bd0a9a5342e75354536',
      accounts: ['0x40a20905cacfc7f174271928c79cee13ad836348c607a57f53165956399345e8'],
    },
  },
  gasReporter: {
    currency: 'EUR',
    coinmarketcap: 'ab21c9c3-82ba-4096-85a3-649fef63a4af',
  },
};
